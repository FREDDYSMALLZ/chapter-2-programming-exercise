
public class ComputeArea {
	//Computing the area of a circle

	public static void main(String[] args) {
		double radius;
		double area;
		radius = 20.0;
		
		area = radius * radius * 3.14159;
		
		System.out.println("The area for the circle of radius " + radius + " is " + area + " units ");
		 
	}

}
