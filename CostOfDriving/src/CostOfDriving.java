import java.util.Scanner;
public class CostOfDriving {

	public static void main(String[] args) {
		//Program that prompts the user to enter the distance, fuel efficiency and the miles per gallon
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter the distance to drive
		System.out.print("Enter the distance to drive: ");
		double distance = input.nextDouble();
		
		//Prompt the user to enter the fuel effieciencey in miles per gallon of the car
		System.out.print("Enter miles per gallon: ");
		double milesPerGallon = input.nextDouble();
		
		//Prompt the user to enter the price per gallon
		System.out.print("Enter price per gallon: ");
		double pricePerGallon = input.nextDouble();
		
		double costOfTrip = pricePerGallon * ( distance / milesPerGallon);
		
		System.out.println("The cost of driving is $" + costOfTrip);
		
	}

}
