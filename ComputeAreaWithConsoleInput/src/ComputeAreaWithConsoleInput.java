import java.util.Scanner;
public class ComputeAreaWithConsoleInput {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//Prompt the user to enter the radius of the circle
		
		System.out.print("Enter the radius of the circle: ");
		double radius = input.nextDouble();
		
		//Compute the area of the circle
		double area = radius * radius * 3.14159;
		
		System.out.println("The area of the circle radius " + (double) radius + " is " + area + " units ");
		
		
	}

}
