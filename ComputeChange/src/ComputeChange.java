import java.util.Scanner;
public class ComputeChange {

	public static void main(String[] args) {
		//A program that breaks a large amount of money into smaller units
		
		Scanner input = new Scanner(System.in);
		 //Prompt the user to enter the amount as a decimal
		
		System.out.print("Enter an amount in double: ");
		 double amount = input.nextDouble();
		 
		 int remainingAmount = (int)(amount * 100);
		 
		 
		 //find the number of dollars
		 int numberOfDollars = remainingAmount / 100;
		 remainingAmount = remainingAmount % 100;
		 
		 double numberOfQuarters = remainingAmount / 25;
		 remainingAmount = remainingAmount % 25;
		 
		 int numberOfDimes = remainingAmount / 10;
		 remainingAmount = remainingAmount % 10;
		 
		 int numberOfNickels = remainingAmount / 5;
		 remainingAmount = remainingAmount % 5;
		 
		 int pennies = remainingAmount;
		 
		 System.out.println("The Customer amount " + amount + " consists of ");
		 System.out.println(" " + numberOfDollars + " Dollars ");
		 System.out.println(" " + numberOfQuarters + " Quarters ");
		 System.out.println(" " + numberOfDimes + " Qimes ");
		 System.out.println(" " + numberOfNickels + " Nickels ");
		 System.out.println(" " + pennies + " Pennies ");
	}

}
