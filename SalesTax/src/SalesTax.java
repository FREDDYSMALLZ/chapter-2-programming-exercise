import java.util.Scanner;
public class SalesTax {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		//Promt the user to enter the purchase amount
		System.out.print("Enter the purchase amount: ");
		double purchaseAmount = input.nextDouble();
		
		 //The sales tax is 6% of the purchase amount
		double tax = purchaseAmount * 0.06;
		 System.out.println("The sales tax is $ " + (int)(tax * 100) / 100.0);
	}

}
