import java.util.Scanner;
public class SumDigitsInteger {

	public static void main(String[] args) {
		// Program that reads an integer between 0 and 1000 and adds all the digits to the integer
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter a number between 0 and 1000
		System.out.print("Enter a number between 0 to 1000: ");
		int number = input.nextInt();
		
		int firstDigit = number % 10; // the % operator extracts the digits. for example 932 % 10 = 2
        int remainingNumber = number / 10; // the / operator removes the extracted digit. 932 / 10 = 93
        int secondDigit = remainingNumber % 10;
        remainingNumber = remainingNumber / 10;
        int thirdDigit = remainingNumber % 10;

        int sum = firstDigit + secondDigit + thirdDigit;

        System.out.println("The sum of all digits in " + number + " is " + sum);
		

	}

}
