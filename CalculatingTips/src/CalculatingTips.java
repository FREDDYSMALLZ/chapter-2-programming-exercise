import java.util.Scanner;
public class CalculatingTips {

	public static void main(String[] args) {
		// program that reads subtotal and calculates tips
		
		Scanner input = new Scanner(System.in);
		
		//prompt the user to enter the subtotal
		System.out.print("Enter the subtotal: ");
		double subtotal = input.nextDouble();
		
		//Prompt the user to enter the graturity rate
		System.out.print("Enter the graturity rate: ");
		double graturityRate = input.nextDouble();
		
		double graturity = ( graturityRate / 100 )  * subtotal;
		double total = subtotal + graturity;
		
		System.out.println("The graturity is $" + graturity + " and " + " total is $" + total);

	}

}
