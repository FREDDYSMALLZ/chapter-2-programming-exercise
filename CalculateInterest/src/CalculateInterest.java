import java.util.Scanner;
public class CalculateInterest {

	public static void main(String[] args) {
		//Program that calculates interest
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter the balance
		System.out.print("Enter the balance: ");
		double balance = input.nextDouble();
		
		//Prompt the user to enter the interest rate
		System.out.print("Enter the interest rate: ");
		double annualinterestRate = input.nextDouble();
		
		double interest = balance * ( annualinterestRate / 1200 );
		
		System.out.println("The interest is$" + interest);

	}

}
