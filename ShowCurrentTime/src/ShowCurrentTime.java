import java.util.Scanner;
public class ShowCurrentTime {

	public static void main(String[] args) {
		//Develop a program that displays the currenyt time in GreenWich Mean Time (GMT)
		Scanner input = new Scanner(System.in);
		
		long totalMilliseconds = System.currentTimeMillis(); //Obtains total time in Milliseconds
		long totalSeconds = totalMilliseconds / 1000;//Obtains the the total seconds
		long currentSecond = totalSeconds % 60; //Obtains the current second
		long totalMinutes = totalSeconds / 60; //Obtains the total minute
		long currentMinute = totalMinutes % 60; //Obtains the current minute
		long totalHours = totalMinutes / 60; //Obtains the total hours
		long currentHour = totalHours % 24; //Obtains the current hour
		
		System.out.println("Current time is: " + currentHour + ":" + currentMinute + ":" +  currentSecond + " GMT ");
		
	}

}
