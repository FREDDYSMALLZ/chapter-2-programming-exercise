import java.util.Scanner;
public class CelciusToFerenheit {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter degrees in celcius
		System.out.print("Enter degrees in celcius: ");
		double celcius = input.nextDouble();
		
		double farenheit = (9.0 / 5 ) * celcius + 32;
		
		System.out.println(celcius + " Celcius is " + farenheit + " Farenheit. ");

	}
}
