import java.util.Scanner;
public class ConvertPoundsToKilograms {

	public static void main(String[] args) {
		// Program that convert pounds to kilograms
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the usere to enter the number in pounds
		System.out.println("Enter the number in pounds: ");
		 double pounds = input.nextDouble();
		 
		 double kilograms = pounds * 0.454;//One pound = 0.454 kilograms
		 
		 System.out.println(pounds + " pounds " + " is " + kilograms + " kilograms.");

	}

}
