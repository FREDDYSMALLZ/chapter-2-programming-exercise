import java.util.Scanner;
public class VolumeOfCylinder {

	public static void main(String[] args) {
		//Program to compute the volume of a cylinder
		//Program to compute the area of a cylinder
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter the radius of the cylinder
		System.out.print("Enter the radius of the cylinder: ");
		double radius = input.nextDouble();
		
		//Prompt the user to enter the length of the cylinder
		System.out.print("Enter the lenth of the cylinder: ");
		double length = input.nextDouble();
		
		double area = radius * radius * Math.PI; //Calculate the area of the cylinder
		double volume = area * length; //Calculate the volume of the cylinder
		
		System.out.println("The area of the cylinder " + " is:" + area);
		System.out.println("The volume of the cylinder is:" + volume);
		

	}

}
