import java.util.Scanner;
public class AreaOfTriangle {

	public static void main(String[] args) {
		//Program that displays the area of a triangle by entering three points
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter three points of the triangle
		System.out.print("Enter three points for a triangle:");
		  double x1 = input.nextDouble();//(x1,y1) gives side p of the triangle
		  double y1 = input.nextDouble();
		  double x2 = input.nextDouble();//(x2,y2) gives side q of the triangle
		  double y2 = input.nextDouble();
		  double x3 = input.nextDouble();//(x3,y3) gives side k of the triangle
		  double y3 = input.nextDouble();
		 
		  double sideP = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
		  double sideQ = Math.sqrt(Math.pow(x2 - x3, 2) + Math.pow(y2 - y3, 2));
		  double sideK = Math.sqrt(Math.pow(x1 - x3, 2) + Math.pow(y1 - y3, 2));
		  
		  double s = (sideP + sideQ + sideK) / 2;
		  double area = Math.sqrt(s * (s - sideP) * (s - sideQ) * (s - sideK));
		 
		  System.out.print("The area of the triangle is  " + area);
		 

	}

}
