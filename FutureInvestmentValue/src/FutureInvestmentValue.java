import java.util.Scanner;
public class FutureInvestmentValue {

	public static void main(String[] args) {
		// Program that displays the future investment value
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter an investment amount
		System.out.print("Enter an Investment amount: ");
		double investmentAmount = input.nextDouble();
		
		//Prompt the user to enter the annual interest rate
		System.out.print("Enter the annual Interst rate as a percentage: ");
		double annualInterestRate = input.nextDouble();
		double monthlyInterestRate = annualInterestRate / 1200;
		
		//Prompt the user to enter the number of years
		System.out.print("Enter the number of years: ");
		double numberOfYears = input.nextInt();
		
		double futureInvestmentValue = investmentAmount * ( 1 + Math.pow(monthlyInterestRate, numberOfYears ) * 12);
		
		System.out.println("The future investment value is $" + futureInvestmentValue);
		

	}

}
