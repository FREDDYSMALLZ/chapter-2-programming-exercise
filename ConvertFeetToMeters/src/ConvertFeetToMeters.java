import java.util.Scanner;
public class ConvertFeetToMeters {

	public static void main(String[] args) {
		// Create a program to covert feet to meters 
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter the number in feet
		System.out.print("Enter the number in feet: ");
		double feet = input.nextDouble();
		
		double meters = feet * 0.305;//one foot = 0.305 meters
		
		System.out.println(feet + " feet " + " is " + meters + " meters. " );
		

	}

}
