import java.util.Scanner;
public class ComputeLoan {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter the annual inerest rate
		System.out.print("Enter the annual interest rate: ");
		double annualInterestRate = input.nextDouble();
		
		double monthlyInterestRate = annualInterestRate / 1200;
		
		//Prompt the user to enter the number of years
		System.out.print("Enter the number of years as an integer: ");
		int numberOfYears = input.nextInt();
		
		//Prompt the user to enter the Loan amount
		System.out.print("Enter the loan amount: ");
		double loanAmount = input.nextDouble();
		
		double monthlyPayment = (loanAmount * monthlyInterestRate) / 1 - 
				1 / Math.pow( 1 + monthlyInterestRate, numberOfYears * 12);
		double totalPayment = monthlyPayment * numberOfYears * 12;
		
		System.out.println("The Monthly payment is $" + (int)(monthlyPayment * 100 ) / 100.0);
		System.out.println("The Total payment is $" + (int)(totalPayment * 100 ) / 100.0);
		
		

	}

}
