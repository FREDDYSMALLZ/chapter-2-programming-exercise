import java.util.Scanner;
public class ComputeAreaWithConstant {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		final double PI = 3.14159;
		//Prompt the user to enter the radius of the circle
		System.out.print("Enter the radius of the circle:");
		double radius = input.nextDouble();
		
		double area = radius * radius * PI;
		
		System.out.println("The area of the circle radius " + radius + " is " + area + " units ");
		
		
	}

}
